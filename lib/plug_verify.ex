defmodule SimpleJwtWs.Plug.Verify do
  require Logger

  @jwt_regex ~r/^Bearer (?<jwt>[\w-=]+\.[\w-=]+\.?[\w-.+\/=]*)$/
  
  import Plug.Conn

  defmodule UnauthorizedRequestError do
    @moduledoc """
    Error raised when a required header is missing.
    """
    defexception message: "JWT authorizae fail", plug_status: 403
  end

  defmodule JWTFormatError do
    @moduledoc """
    Error raised when a required header is missing.
    """
    defexception message: "Error JWT Format", plug_status: 401
  end
  
  defmodule JWTRequestError do
    @moduledoc """
    Error raised when a required header is missing.
    """
    defexception message: "Req missing JWT field", plug_status: 401
  end
  
  
  def init(jwt_secret), do: jwt_secret

  def call(%Plug.Conn{method: method,path_info: path_info} = conn, opts) do
    is_verifypath = not is_nil(Enum.find(path_info,fn e -> e == "verify" end))
    if method == "GET" and is_verifypath do 
      case conn |> get_req_header("authorization") do
        [bearer] ->
          case Regex.named_captures(@jwt_regex, bearer) do
            %{"jwt" => jwt} ->
              case JsonWebToken.verify(jwt, opts) do
                {:ok, claims} ->
                  conn
                  |> put_req_header("loginid",claims[:loginid])
                  |> put_req_header("userid",claims[:userid])
                  |> IO.inspect
                _ ->
                  raise(UnauthorizedRequestError)
              end
            _ ->
              raise(JWTFormatError)
          end
        _ ->
          raise(JWTRequestError)
      end
    else
      conn
    end
  end
end
