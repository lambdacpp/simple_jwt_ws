defmodule SimpleJwtWs.Router.HomePage do
  require Logger

  use Maru.Router

  use OdbcConn
  
  plug CORSPlug
  plug SimpleJwtWs.Plug.Verify,
    %{alg: "HS256", key: Application.get_env(:simple_jwt_ws, :jwt_secret)}

  version "v1"
  get do
    conn
    |> json(%{version: "v1", info: "Simple JWT"})
  end

  namespace :verify do
    desc "JWT 校验"
    get do
      conn
      |> json(%{userid: get_header_value(conn,"userid")})
    end

    namespace :userinfo do
      get do
        conn |> json(get_user_info(get_header_value(conn,"userid")))
      end
    end

    namespace :userrole do
      get do
        conn |> json(get_user_role(get_header_value(conn,"userid")))
      end
    end

    namespace :studinfo do
      get do
        conn |> json(get_stud_info(get_header_value(conn,"loginid")))
      end
    end
  end

  namespace :auth do
    desc "账号认证"
    ##
    params do
      requires :uid, type: :string, regexp: ~r/^W?\d+$/
      requires :password, type: :string
    end
    post do
      Logger.debug "auth user :#{params[:uid]}."
      
      user = get_auth_info(params[:uid])
      md5_pwd = :crypto.hash(:md5,params[:password]) |> Base.encode16 |> String.downcase 

      cond do
        user == :notfound -> 
          conn |> json(%{result: :fail, info: "用户名或密码错误"})
          
          user[:password] != md5_pwd ->
          Logger.info "SSO auth fail #{params[:password]} <> #{md5_pwd} [#{params[:password]}]."
          conn |> json(%{result: :fail, info: "用户名或密码错误"}) 
        true ->
          Logger.info "SSO auth success #{params[:uid]}."
          user |> IO.inspect
          token = user
          |> Map.delete(:password)
          |> JsonWebToken.sign(%{key: Application.get_env(:simple_jwt_ws, :jwt_secret)})
          conn |> json(%{result: :success, jwt: token})
      end
    end
  end

  
  deffetchrow get_auth_info(username) do
    '''
    select to_char(USERID) as USERID, 
           LOGINID, ALIAS, PASSWORD,
           to_char(SYSDATE,'YYYY-MM-DD')  as issuetime
      from UIADATA.UT_USERS
     where STATUS=1 and
           (LOGINID ='#{username}' or ALIAS='#{username}')
    '''
  end

  deffetchrow get_user_info(userid) do
    '''
    select LOGINID, ALIAS,CARDID,
           convert(NAME,'UTF8','ZHS16CGB231280') NAME 
      from UIADATA.UT_USERS
     where USERID=#{userid} 
    '''
  end

  deffetchrow get_user_role(userid) do
    '''
    select R.ROLEIDENTIFY as USERROLE
      from UT_ROLES R, UT_USER_ROLE U 
     where R.ROLEID=U.ROLEID and 
           U.USERID=#{userid};
    '''
  end

  deffetchrow get_stud_info(studid) do
    '''
    select DQSZJ as NJ , xz as XZ  
      from dbm.XSJBSJZLB 
     where xh='#{studid}'  
    union 
    select sznj as NJ, '3' as XZ 
      from dbm.cdu_yjs_xsjbsj 
     where xh='#{studid}';
    ''' 
  end

  defp get_header_value(conn,id) do
    List.first(Plug.Conn.get_req_header(conn, id))
  end
end


defmodule SimpleJwtWs.API do
  use Maru.Router
  before do
    plug Plug.Parsers,
      pass: ["*/*"],
      json_decoder: Poison,
      parsers: [:urlencoded, :json, :multipart]
  end

  mount SimpleJwtWs.Router.HomePage

  rescue_from :all, as: e do
    status = Map.get(e,:plug_status,500)
    message = Map.get(e,:message,"Server Error")
    e |> IO.inspect
    conn
    |> IO.inspect
    |> put_status(status)
    |> text(message)
  end
end


