defmodule SimpleJwtWsTest do
  use ExUnit.Case
end

defmodule RootTest do
  use ExUnit.Case
  use Maru.Test, root: SimpleJwtWs.API

  @testuid "201610421218"
  @testpw "141125"

  test "/v1/" do
    assert "{\"info\":\"Simple JWT\",\"version\":\"v1\"}" = get("/v1") |> text_response
  end

  # test "/auth/" do
  #   req = post_data_as_json "/v1/auth", %{uid: @testuid, password: @testpw}
  #   assert "success" = req["result"]  
  # end

  test "/verify/ with jwt" do
    req = post_data_as_json "/v1/auth", %{uid: @testuid, password: @testpw}
    assert "success" = req["result"]  

    jwt = req["jwt"]

    userinfo = build_conn()
    |> Plug.Conn.put_req_header("authorization", "Bearer "<>jwt)
    |> get("/v1/verify")
    |> json_response
    |> IO.inspect
    assert "766987" = userinfo["userid"]

    userinfo = build_conn()
    |> Plug.Conn.put_req_header("authorization", "Bearer "<>jwt)
    |> get("/v1/verify/userinfo")
    |> json_response
    |> IO.inspect
    assert "王洁" = userinfo["name"]
    
    userrole = build_conn()
    |> Plug.Conn.put_req_header("authorization", "Bearer "<>jwt)
    |> get("/v1/verify/userrole")
    |> json_response
    |> IO.inspect
    assert "ROLE_STUDENT" = userrole["userrole"]

    studinfo = build_conn()
    |> Plug.Conn.put_req_header("authorization", "Bearer "<>jwt)
    |> get("/v1/verify/studinfo")
    |> json_response
    |> IO.inspect
    assert "2017" = studinfo["nj"]
    assert "4" = studinfo["xz"]
  end

  
  test "/verify/" do
    assert "Req missing JWT field" = get("/v1/verify") |> text_response
  end

  defp post_data_as_json(url,data) do 
    build_conn()
    |> Plug.Conn.put_req_header("content-type", "application/json")
    |> put_body_or_params(Poison.encode!(data))
    |> post(url)
    |> json_response
    |> IO.inspect
  end
    
end
  
