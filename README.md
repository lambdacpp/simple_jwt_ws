# `SimpleJwtWs` 最简单的JWT服务接口

`SimpleJwtWs` 实现最简单的JWT服务，提供认证和校验接口。

## 版本

- v0.1 实现最简单的JWT服务，有手机校验码接口
- v0.2 使用odbc_conn库，去掉手机校验码接口
- v0.3 提供校验服务


## 接口说明

### 认证接口

```
$curl --data "uid=201610421218&password=141125" http://localhost:8000/v1/auth

{"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiI3NjY5ODciLCJsb2dpbmlkIjoiMjAxNjEwNDIxMjE4IiwiYWxpYXMiOiIyMDE2MTA0MjEyMTgifQ.ly4iF13rGggAYY6wVMAXig9iAg-UcP6ZCoEb5IVIgyI","result":"success"}
```

### 校验接口

在`GET`请求中添加`authorization`字段，可获取用户信息

```
curl -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiI3NjY5ODciLCJsb2dpbmlkIjoiMjAxNjEwNDIxMjE4IiwiYWxpYXMiOiIyMDE2MTA0MjEyMTgifQ.ly4iF13rGggAYY6wVMAXig9iAg-UcP6ZCoEb5IVIgyI" http://172.16.190.138:8000/v1/verify 
{"userid":"766987"}
```

具体功能如下：
- `/v1/verify/userinfo` 获取用户信息
- `/v1/verify/userrole` 获取用户角色
- `/v1/verify/studinfo` 获取学生年级和学制



